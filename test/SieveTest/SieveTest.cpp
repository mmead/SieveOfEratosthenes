//*******************************************************************************
// Author: Michael Mead
//
// Copyright 2017 Michael Mead
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//*******************************************************************************

#include <SieveOfEratosthenes.h>

#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>

using namespace std;

//*******************************************************************************
//*******************************************************************************
void RunTest()
{
  SieveOfEratosthenes sieve;

  vector<unsigned> actualPrimes = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37};
  auto generatedPrimes = sieve.GetPrimesStrictlyLessThan(41);

  if (generatedPrimes->size() != actualPrimes.size())
  {
    throw runtime_error("RunTest: Generated wrong number of primes.");
  }

  for (size_t i = 0; i < actualPrimes.size(); ++i)
  {
    unsigned actualPrime = actualPrimes[i];
    unsigned generatedPrime = generatedPrimes->at(i);

    if (generatedPrime != actualPrime)
    {
      cerr
        << "Expected: " << actualPrime << '\n'
        << "Got: " << generatedPrime << endl;

      throw runtime_error("RunTest: Failed to find primes < 41.");
    }
  }
}

//*******************************************************************************
//*******************************************************************************
int main(int argc, char** argv)
{
  cout << "Starting SieveTest..." << endl;

  try
  {
    RunTest();
  }
  catch (const runtime_error& Error)
  {
    cerr << "ERROR: " << Error.what() << endl;
    return 1;
  }

  cout << "Passed." << endl;
  return 0;
}
