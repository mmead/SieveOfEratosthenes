#--------------------------------------------------------------------------------
# SieveTest
#--------------------------------------------------------------------------------
add_executable(SieveTest
  SieveTest.cpp)

target_link_libraries(SieveTest
  libSieveOfEratosthenes)

add_test(SieveTest SieveTest)
