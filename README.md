# SieveOfEratosthenes

## Overview
An object oriented solution to the famous mathematical algorithm Sieve of Eratosthenes, which finds all prime numbers
under a given integer value. Read more at https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes

The particular implementation finds all primes *strictly less* than a given integer value.

## Implementation goals
The program provides a flexible solution to the proposed problem by encapsulating the implementation details inside an object. This allows for composition of the `SieveOfEratosthenes` in future programs that could benefit from being able to find prime numbers. It was also a goal of the program to provide a class that is extensible for future developers to use -- especially if their problem is not specifically to find prime numbers. Examples of this include finding composite numbers or getting all primes between a certain range.

## Algorithm details
There are several interesting ways to solve the problem of finding prime numbers. A key assumption made in the algorithm is that the problem fits in memory. That is not to say that the algorithm should be overzealous in its memory consumption. Efficient (and small) data structures are used to handle the proposed problem.

The solution implemented uses a mask of `unsigned char` (1 byte each) to keep track of if a number is prime or composite. We avoid explicitly allocating any numbers, dubbed "candidates", by using indices of the mask to keep track of candidates.

All elements of the mask start as being prime numbers and are subsequently tested.

The algorithm works by iterating through the mask, starting at index `2`, since `2` is the first prime number. If the current mask element is set to prime, the algorithm then goes through the mask and sets all multiples of the current element to be composite. We also save considerable computation time by marking multiples starting at the square of the current index. We do this by noticing that *after* 2, the first *unmarked* multiple appears at its square. This is because all previous multiples will have already been marked as an multiple of an element before it. (This is actually taking advantage of integer factorization -- a really cool way to reduce computation in a number of applications.)

We continue to iterate through the mask and mark multiples until we reach a point where the square of the current candidate is greater than the number of elements in the mask. When this happens we know that we cannot mark any more multiples, since the sequence is strictly monotonic increasing and any subsequent candidate will have an even larger square. Thus, the remaining elements that are unmarked must all be prime. We stop computation at this point.

Finally, to provide the client of the code with a array of prime numbers, we iterate through the mask and check the prime status of the current mask element; if the status is set to the prime value (e.g. true), we add that element to an array. The array is stored as a `std::unique_ptr` and owernship is trasferred to client after the function returns. This avoids an expensive copy for large `N`.

## Build support

The project uses [CMake](https://cmake.org/) for cross platform builds, and
was tested using version 3.8.0 on Fedora 25. It also uses CMake for unit
testing.

To save a few keystrokes, you can use the automated build and run shell scripts
located under `util`. Please note that the build scripts are expected to be run
from the project root directory.

### Build

Builds the code under the project root directory in a directory called `build`.
Installs the executable under the project root directory in a directory called `install`.

```bash
$ cd SieveOfEratosthenes
$ sh util/Build.sh
```

### Run

Runs the executable under `install/bin`.

```bash
$ cd SieveOfEratosthenes
$ sh util/Run.sh
```

### Test

Runs the unit tests.

```bash
$ cd SieveOfEratosthenes
$ sh util/RunTests.sh
```

## License

Copyright © Michael Mead, since 2017.

Distributed under the MIT license. Please see the LICENSE file for details.
