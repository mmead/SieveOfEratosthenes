//*******************************************************************************
// Author: Michael Mead
//
// Copyright 2017 Michael Mead
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//*******************************************************************************

#include <iostream>
#include <limits>
#include <memory>
#include <stdexcept>

#include <SieveOfEratosthenes.h>

//*******************************************************************************
//*******************************************************************************
void RunTest(unsigned maxPrime)
{
  SieveOfEratosthenes sieve;
  auto pPrimes = sieve.GetPrimesStrictlyLessThan(maxPrime);

  std::cout
    << "Found " << pPrimes->size() << " prime numbers "
    << "strictly less than " << maxPrime
    << '\n';

  for (auto It = pPrimes->begin(); It != pPrimes->end(); ++It)
  {
    std::cout << *It << '\n';
  }

  std::cout << std::endl;
}

//*******************************************************************************
//*******************************************************************************
unsigned GetMaxPrimeFromUser()
{
  std::cout
    << "Please enter a positive number to find all the "
    << "prime numbers strictly less than: ";


  int maxPrime = 0;
  std::cin >> maxPrime;

  if (std::cin.fail() || maxPrime < 0)
  {
    std::string ErrorMessage("GetMaxPrimeFromUser:");
    ErrorMessage.append("\n");
    ErrorMessage.append("Got bad option. Enter a positive integer.");
    throw std::runtime_error(ErrorMessage);
  }

  std::cout << '\n';
  return static_cast<unsigned>(maxPrime);
}

//*******************************************************************************
//*******************************************************************************
void WaitForUserToQuit()
{
  std::cout << "Press Enter to exit." << std::endl;
  std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
  std::cin.get();
}

//*******************************************************************************
//*******************************************************************************
int main(int argc, char ** argv)
{
  try
  {
    unsigned maxPrime = GetMaxPrimeFromUser();
    RunTest(maxPrime);
  }
  catch (const std::runtime_error& Error)
  {
    std::cout << Error.what() << std::endl;
    WaitForUserToQuit();
    return 1;
  }

  WaitForUserToQuit();
  return 0;
}
