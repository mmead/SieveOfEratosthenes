//*******************************************************************************
// Author: Michael Mead
//
// Copyright 2017 Michael Mead
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//*******************************************************************************

#include "SieveOfEratosthenes.h"

const unsigned char SieveOfEratosthenes::mNonPrimeValue = 0;
const unsigned char SieveOfEratosthenes::mPrimeValue = 1;

//*******************************************************************************
//*******************************************************************************
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
SieveOfEratosthenes::SieveOfEratosthenes()
  : mPrimesMask()
{
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
std::unique_ptr<std::vector<unsigned>>
SieveOfEratosthenes::GetPrimesStrictlyLessThan(unsigned maxPrime)
{
  ResetMask(maxPrime);
  FindPrimes();
  return std::move(GetPrimesFromMask());
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
bool SieveOfEratosthenes::IsPrime(unsigned candidate) const
{
  if (mPrimesMask.empty())
  {
    return false;
  }

  return mPrimesMask[candidate - 1] == mPrimeValue;
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
bool SieveOfEratosthenes::IsMultipleOf(
  unsigned candidate,
  unsigned potentialMultiple) const
{
  return potentialMultiple % candidate == 0;
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
void SieveOfEratosthenes::MarkMultiplesOf(unsigned candidate)
{
  for (
      unsigned potentialMultiple = candidate * candidate;
      potentialMultiple < mPrimesMask.size();
      ++potentialMultiple)
  {
    if (IsMultipleOf(candidate, potentialMultiple))
    {
      MarkNonPrime(potentialMultiple);
    }
  }
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
void SieveOfEratosthenes::MarkNonPrime(unsigned nonPrime)
{
  if (!mPrimesMask.empty())
  {
    mPrimesMask[nonPrime - 1] = mNonPrimeValue;
  }
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
void SieveOfEratosthenes::ResetMask(unsigned maxPrime)
{
  mPrimesMask.resize(maxPrime, mPrimeValue);
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
void SieveOfEratosthenes::FindPrime(unsigned candidate)
{
  if (IsPrime(candidate))
  {
    MarkMultiplesOf(candidate);
  }
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
void SieveOfEratosthenes::FindPrimes()
{
  for (unsigned candidate = 2; candidate < mPrimesMask.size(); ++candidate)
  {
    FindPrime(candidate);
  }
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
std::unique_ptr<std::vector<unsigned>>
SieveOfEratosthenes::GetPrimesFromMask() const
{
  auto pPrimes = std::unique_ptr<std::vector<unsigned>>(
    new std::vector<unsigned>());

  for (unsigned candidate = 2; candidate < mPrimesMask.size(); ++candidate)
  {
    if (IsPrime(candidate))
    {
      pPrimes->push_back(candidate);
    }
  }

  return std::move(pPrimes);
}
